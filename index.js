


const bookList = document.querySelector(".book-list");
const bookForm = document.querySelector(".book-form");
const containerForm = document.querySelector(".container")

class Book {
    constructor(title, author, date) {
        this.title = title;
        this.author = author;
        this.date = date;
    }

    //Methode 1
    //Création de la méthode de la création d'un livre
    addBookList(book) {
        //création de la ligne 
        const row = document.createElement("tr");
        row.innerHTML = `
        <td>${book.title}</td>
        <td>${book.author}</td>
        <td>${book.date}</td>
        <td><button class="delete">x</button></td>`;
        //Intégration de la rangée dans le DOM
        bookList.appendChild(row);
    }

    //Methode 2
    //Suppression des champs du formulaire une fois le livre enregistré
    clearField() {
        document.getElementById("title").value = "";
        document.getElementById("author").value = "";
        document.getElementById("date").value = "";
    }

    //Methode 3
    //Déclaration d'un message d'alerte
    showAlert(message, className) {
        const alert = document.createElement("div");
        //Création de deux classes : alerte et error ou success
        alert.className = `alert ${className}`;
        console.log(alert.className);
        const text = document.createTextNode(message);
        alert.appendChild(text);
        //insertion de la div avant bookForm
        containerForm.insertBefore(alert, bookForm);

        setTimeout(() => {
            document.querySelector(".alert").remove()
        }, 2500)
    }
}

//intégration du nouveau livre dans la liste
bookForm.addEventListener("submit", (e) => {
    //On garde les données en local
    e.preventDefault();

    //On récupère les nouvelles données 
    let title = document.getElementById("title").value;
    let author = document.getElementById("author").value;
    let date = document.getElementById("date").value;

    //création d'un nouveau livre
    let book = new Book(title, author, date);

    //Affichage du message d'erreur ou de succès
    if (title === "" || author === "" || date === "") {
        book.showAlert("Remplissez les champs", "error");
    } else {
        //Intégration de ce nouveau livre dans la liste
        book.addBookList(book);
        //Effacement des champs
        book.clearField();
        //Message de succès
        book.showAlert("Livre ajouté", "success");
    }

})

//Création de la fonction de delete
class Interface {
    deleteBook(target) {
        if (target.className === "delete") {
            target.parentElement.parentElement.remove();
        }
    }
}

//Suppression de la ligne du livre
bookList.addEventListener("click", (e) => {

    const removeLine = new Interface();
    removeLine.deleteBook(e.target);
})















